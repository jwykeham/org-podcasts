;;; git-utils.el --- Git utility functions

;; Copyright (C) 2021 James Wykeham

;; Author: James Wykeham <jwykeham@gmail.com>
;; URL: https://gitlab.com/jwykeham/org-podcasts
;; Version: 1.0.0
;; Keywords: org

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Org-Podcasts
;;
;; See documentation on https://gitlab.com/jwykeham/org-podcasts

;;; Code:
(require 'org-feed)

(defgroup org-podcasts nil
  "Customizations for org feed to handle podcasts."
  :prefix "org-podcasts-"
  :version "27.2")

(defcustom org-podcasts-file
  "~/org/podcasts.org"
  "File path to add podcasts to."
  :group 'org-podcasts
  :type '(string))


(defun org-podcasts--format-time (time)
  "Format time string."
  (format-time-string "<%Y-%m-%d %a %H:%M>" (encode-time (parse-time-string time))))

(defun org-podcasts--plist-to-spec (data)
  "Return spec."
  (list (cons ?t (org-podcasts--escape-cdata (plist-get data :title)))
	(cons ?e (plist-get data :enclosureUrl))
	(cons ?p (org-podcasts--format-time (plist-get data :pubDate)))
	(cons ?d (org-podcasts--pandoc-html-to-org (plist-get data :description)))))

(defun org-podcasts--formatter (entry)
  "Format entry."
  (format-spec "* LISTEN %t\n:PROPERTIES:\n:URL: %e\n:END:\n%p\n%d" (org-podcasts--plist-to-spec entry)))

(defun org-podcasts--escape-cdata (str)
  "Escape CDATA tag."
  (save-match-data
    (if (string-match "<!\\[CDATA\\[\\([^]+]+\\)\\]\\]>" str)
	(match-string 1 str)
      str)))

(defun org-podcasts--enclosure-to-plist (enclosure-xml-string)
  (let ((root (with-temp-buffer
    (insert enclosure-xml-string)
    (xml-parse-region (point-min) (point-max)))))
    (xml-node-attributes (car root))))

(defun org-podcasts--get-enclosure (xml-str)
  (save-match-data
    (string-match "<enclosure [^>]+/>" xml-str)
    (match-string 0 xml-str)))

(defun org-podcasts--entry-parser (entry)
  (org-feed-parse-rss-entry entry)
  (plist-put entry ':enclosureUrl (format "%s" (cdr (assoc 'url (org-podcasts--enclosure-to-plist (org-podcasts--get-enclosure (plist-get entry :item-full-text)))))))
  entry)

(defun org-podcasts--org-feed-item (title url)
  (list title url org-podcasts-file title :formatter 'org-podcasts--formatter :parse-entry 'org-podcasts--entry-parser))


;; This will lock emacs I can make a async version with (start-process...) (send-to-process...)
;; But not sure how to make that work with the formatter and org-feed-update
(defun org-podcasts--pandoc-html-to-org (html)
  "Use pandoc to convert html into org markdown."
  (with-temp-buffer
    (insert (org-podcasts--escape-cdata html))
    (shell-command-on-region
     (point-min)
     (point-max)
     "pandoc --from=html --to=org"
     (current-buffer)
     t
     "*Pandoc Error Buffer*"
     t)
    (encode-coding-string (buffer-substring-no-properties (point-min) (point-max)) 'utf-8)))

(defun org-podcasts-org-add (title url)
  "Add a Podcast to org feed."
  (customize-push-and-save 'org-feed-alist (list (org-podcasts--org-feed-item title url)))
  (org-feed-update title))

(defun org-podcasts-org-add-p (URL title)
  "Add podcast interactively."
  (interactive "sURL: \nsTitle: ")
  (org-podcasts-org-add title URL))

(defun org-podcast-emms-play ()
  "Play podcast using emms."
  (interactive)
  (emms-play-url (org-element-property :URL (org-element-at-point))))

(defun org-podcast-archive-listened ()
  "Mark Listened podcasts as Archived"
  (interactive)
  (org-map-entries '(org-toggle-tag "ARCHIVE" 'on) "/+LISTENED" '("~/org/podcasts.org") 'archive 'comment))

(provide 'org-podcasts)
;;; org-podcasts.el ends here
